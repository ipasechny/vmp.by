﻿
/*[===> UPDATES <===]*/
$(function() {
	
	/*[Main menu <]*/
	$('.header .menu > ul > li').each(function(index, element) {
		if ($(this).children('ul').length) {
			$(this).addClass('has-submenu');
		};
	});
	
	$('.header .menu > ul > li.has-submenu > a').on('click', function(e) {
		if ($(window).innerWidth() <= 980) {
			var item = $(this).parent('li');
			if (!item.hasClass('open')) {
				item.addClass('open').siblings().removeClass('open');
			} else {
				item.removeClass('open');
			}; return false;
		};
	});
	
	$(document).on('click', function(e) {
		if ($(e.target).closest('.header .menu').length == 0) {
			$('.header .menu > ul > li').removeClass('open');
		};
	});
	/*[Main menu >]*/
	
	/*[Controlls <]*/
	$('.header .controlls .trigger').on('click', function(e) {
		$('.header .controlls').toggleClass('show');
		return false;
	});
	
	$(document).on('click', function(e) {
		if ($(e.target).closest('.header .controlls').length == 0) {
			$('.header .controlls').removeClass('show');
		};
	});
	/*[Controlls >]*/
	
	/*[Main slider <]*/
	$('.main-slider .slides').slick({
		fade: true,
		dots: true,
		speed: 800,
		arrows: false,
		autoplay: true,
		infinite: true,
		draggable: false,
		autoplaySpeed: 3000
	});
	/*[Main slider >]*/
	
	/*[Page options <]*/
	$('.page-options select').selectOrDie({
		links: true,
		placeholderOption: true
	});
	/*[Page options >]*/
	
});
/*[===> UPDATES <===]*/

$(document).ready(function() {
	// Выбор года
	$('.yearSelect li a').click(function(){
		if(!$(this).parent('li').is('.current')){
			$(this).parents('.yearSelect:last').find('li.current').removeClass('current');
			$(this).parents('.yearSelect:last').removeClass('activeSelect');
			$(this).parent('li').addClass('current');
		}else{
			$(this).parents('.yearSelect:last').toggleClass('activeSelect');
		}
		return false;
	});
	$(document).click(function (e) {
		if (!$(e.target).parents('.yearSelect').length) {
			$('.yearSelect.activeSelect li.current a').click();
		}
	});
	
	$('.person .more a').click(function(){
		$(this).parents('.person').toggleClass('open');
		return false;
	})
});